<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <title></title>
    <link rel="stylesheet" href="<c:url value="/resources/css/styles.css"/>">
    <script src="<c:url value="/resources/js/jquery-2.1.0.min.js"/>"></script>
    <script src="<c:url value="/resources/js/scripts.js"/>"></script>
</head>
<body>
<div id="wrapper">
    <jsp:include page="header.jsp"/>
    <h1>Hello MoneyTracker!</h1>
</div>
</body>
</html>
