package org.geekhub.dmitrytarianyk.moneytracker.controllers.api;

import org.geekhub.dmitrytarianyk.moneytracker.entity.Movie;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.xml.ws.Response;

@Controller
@RequestMapping("/api")
public class ApiController {

    @RequestMapping(value = "/movie/{name}", method = RequestMethod.GET)
    public @ResponseBody Movie getMovie(@PathVariable String name) {
        return new Movie(name);
    }
}
